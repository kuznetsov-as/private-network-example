# Разворачиваем виртуальную приватную сеть при помощи Wireguard, Pihole, Mkcert, Nginx Proxy Manager.

## External Network

Чтобы в дальнейшем иметь возможность быстро подключать контейнеры разврернутые в рамках разных docker-compose.yml, а также 
закрыть максимальное количество портов и заставить Nginx Proxy Manager проксировать все запросы с помощью докера - 
нам необходимо создать внешнюю сеть. Далее в эту сеть будут подключаться все контейнеры, которые мы захотим проксировать.

``` bash
docker network create -d bridge --subnet 13.0.0.0/24 asgardnet
```

## Wireguard
Wireguard это единственный наш сервис у которого будет открыт один единственный порт: 51820 - внутри wireguard и 
соответствующий ему на хосте 51870 (меняется на любой другой по своему усмотрению, главное не забыть также указать
его в environment WG_PORT). Этот порт необходим для клиентов Wireguard и закрыть его не получится. Также, для удобства, 
при первом включении открываем 51821 (с помощью проброса на хост в удобный порт, у меня это 51841). Обращаем внимание 
на то, что уже цепляем Wireguard к нашей внешней сети, которую только что создали. Временно выдаем ему ip в указанной
подсети (сейчас это удобно, а вот позже можем избавиться). Не забываем указать WG_HOST (ip сервера) и PASSWORD. Своего 
DNS у нас пока еще нет, поэтому оставляем 1.1.1.1

**docker-compose.yml**

``` bash
version: "3.8"
services:
    wg-easy-test:
        environment:
            # Required:
            # Change this to your host's public address
            - WG_HOST=

            # Optional:
            - PASSWORD=
            - WG_PORT= 51870
            - WG_DEFAULT_ADDRESS= 10.1.0.x
            - WG_DEFAULT_DNS= 1.1.1.1
            # - WG_MTU=1420
            # - WG_ALLOWED_IPS=192.168.15.0/24, 10.0.1.0/24
            # - WG_PRE_UP=echo "Pre Up" > /etc/wireguard/pre-up.txt
            # - WG_POST_UP=iptables -t nat -A POSTROUTING -s 10.8.0.0/24 -o eth0 -j MASQUERADE; iptables -A INPUT -p udp -m udp --dport 51822 -j ACCEPT; iptables -A FORWARD -i wg0 -j ACCEPT; iptables -A FORWARD -o wg0 -j ACCEPT;
            # - WG_PRE_DOWN=echo "Pre Down" > /etc/wireguard/pre-down.txt
            # - WG_POST_DOWN=echo "Post Down" > /etc/wireguard/post-down.txt
        image: weejewel/wg-easy
        # image: antonkuznetsov/wg-easy:amsterdam
        container_name: wg-easy-test
        volumes:
            - ./data:/etc/wireguard
        ports:
            - "51870:51820/udp"
            - "51841:51821/tcp"
        restart: unless-stopped
        cap_add:
            - NET_ADMIN
            - SYS_MODULE
        sysctls:
            - net.ipv4.ip_forward=1
            - net.ipv4.conf.all.src_valid_mark=1
        networks:
            asgardnet:
               ipv4_address: 13.0.0.3

networks:
  asgardnet:
    external: true
    name: asgardnet
```

Поднимаем:
``` bash
docker-compose up -d
```

Теперь заходим на http://yourServerIp:51841 и попадаем в панель wireguard. Проверяем, что все работает и создаем одного клиента. 
Подключаемся как этот клиент через любое устройство к туннелю и идем http://13.0.0.3:51821. Если все работает - 
отлично, можем закрывать порт на хосте:

``` bash
ports:
    - "51870:51820/udp"
#   - "51841:51821/tcp"
```

Перезапускаем:
``` bash
docker-compose down && docker-compose up -d
```

Теперь доступ в веб панель есть только у клиентов wireguard.

## Nginx Proxy Manager

Тут можно сразу закрыть все порты. Главное внимательно посмотреть файл .yml и сконфигурировать все под свои запросы 
(вроде выноса бд в отдельный контейнер или настройки IPV6). Также важно понимать, что в целом, можно развернуть 
контейнер с Nginx Proxy Manager в одном docker-compose.yml с Wireguard, так как по сути эти контейнеры предствляют 
собой единую систему - но это уже дело вкуса, мне удобнее хранить их отдельно. 

**docker-compose.yml**

``` bash
version: '3.8'
services:
  app:
    image: 'jc21/nginx-proxy-manager:latest'
    restart: unless-stopped
    container_name: npm
    #ports:
      # These ports are in format <host-port>:<container-port>
      #- '80:80' # Public HTTP Port
      #- '443:443' # Public HTTPS Port
      #- '9191:81' # Admin Web Port
      # Add any other Stream port you want to expose
      # - '21:21' # FTP

    # Uncomment the next line if you uncomment anything in the section
    # environment:
      # Uncomment this if you want to change the location of
      # the SQLite DB file within the container
      # DB_SQLITE_FILE: "/data/database.sqlite"

      # Uncomment this if IPv6 is not enabled on your host
    environment:
      # Uncomment this if you want to change the location of
      # the SQLite DB file within the container
      # DB_SQLITE_FILE: "/data/database.sqlite"

      # Uncomment this if IPv6 is not enabled on your host
      DISABLE_IPV6: 'true'

    volumes:
      - ./data:/data
      - ./letsencrypt:/etc/letsencrypta
    networks:
      asgardnet:
        ipv4_address: 13.1.0.5

networks:
  asgardnet:
    external: true
    name: asgardnet
```

После того как поднимем, можем сразу подключаться к нему по пути http://13.0.0.5:81 и любоваться.
Дефолтный логин/пароль:

``` bash
Email: admin@example.com
Password: changeme
```

Меняем их в панели настроек на нормальные и пока идем дальше, позже мы к нему вернемся.

## Nginx 
Немного отдохнем и поднимем Nginx для статики (положим туда змейку) 

**docker-compose.yml**
``` bash
version: "3.8"

services:

  nginx:
    image: nginx
    container_name: nginx
    restart: unless-stopped
    # ports:
      #- "9090:80"
    volumes:
      - ./log:/var/nginx/log:ro
      - ./data:/usr/share/nginx/html:ro
    networks:
      asgardnet:

networks:
  asgardnet:
    external: true
    name: asgardnet

```

В data положим файл **index.html** (Только осторожнее, он большой):
<details>
  <summary>Тык</summary>
  
  ``` bash
  <!DOCTYPE html>
  <html>
  <head>
    <title></title>
    <style>
    html, body {
      height: 100%;
      margin: 0;
    }
  
    body {
      background: black;
      display: flex;
      align-items: center;
      justify-content: center;
    }
    canvas {
      border: 1px solid white;
    }
    </style>
  </head>
  <body>
  <canvas width="400" height="400" id="game"></canvas>
  <script>
  var canvas = document.getElementById('game');
  var context = canvas.getContext('2d');
  
  var grid = 16;
  var count = 0;
  
  var snake = {
    x: 160,
    y: 160,
  
    // snake velocity. moves one grid length every frame in either the x or y direction
    dx: grid,
    dy: 0,
  
    // keep track of all grids the snake body occupies
    cells: [],
  
    // length of the snake. grows when eating an apple
    maxCells: 4
  };
  var apple = {
    x: 320,
    y: 320
  };
  
  // get random whole numbers in a specific range
  // @see https://stackoverflow.com/a/1527820/2124254
  function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
  }
  
  // game loop
  function loop() {
    requestAnimationFrame(loop);
  
    // slow game loop to 15 fps instead of 60 (60/15 = 4)
    if (++count < 4) {
      return;
    }
  
    count = 0;
    context.clearRect(0,0,canvas.width,canvas.height);
  
    // move snake by it's velocity
    snake.x += snake.dx;
    snake.y += snake.dy;
  
    // wrap snake position horizontally on edge of screen
    if (snake.x < 0) {
      snake.x = canvas.width - grid;
    }
    else if (snake.x >= canvas.width) {
      snake.x = 0;
    }
  
    // wrap snake position vertically on edge of screen
    if (snake.y < 0) {
      snake.y = canvas.height - grid;
    }
    else if (snake.y >= canvas.height) {
      snake.y = 0;
    }
  
    // keep track of where snake has been. front of the array is always the head
    snake.cells.unshift({x: snake.x, y: snake.y});
  
    // remove cells as we move away from them
    if (snake.cells.length > snake.maxCells) {
      snake.cells.pop();
    }
  
    // draw apple
    context.fillStyle = 'red';
    context.fillRect(apple.x, apple.y, grid-1, grid-1);
  
    // draw snake one cell at a time
    context.fillStyle = 'green';
    snake.cells.forEach(function(cell, index) {
  
      // drawing 1 px smaller than the grid creates a grid effect in the snake body so you can see how long it is
      context.fillRect(cell.x, cell.y, grid-1, grid-1);
  
      // snake ate apple
      if (cell.x === apple.x && cell.y === apple.y) {
        snake.maxCells++;
  
        // canvas is 400x400 which is 25x25 grids
        apple.x = getRandomInt(0, 25) * grid;
        apple.y = getRandomInt(0, 25) * grid;
      }
  
      // check collision with all cells after this one (modified bubble sort)
      for (var i = index + 1; i < snake.cells.length; i++) {
  
        // snake occupies same space as a body part. reset game
        if (cell.x === snake.cells[i].x && cell.y === snake.cells[i].y) {
          snake.x = 160;
          snake.y = 160;
          snake.cells = [];
          snake.maxCells = 4;
          snake.dx = grid;
          snake.dy = 0;
  
          apple.x = getRandomInt(0, 25) * grid;
          apple.y = getRandomInt(0, 25) * grid;
        }
      }
    });
  }
  
  // listen to keyboard events to move the snake
  document.addEventListener('keydown', function(e) {
    // prevent snake from backtracking on itself by checking that it's
    // not already moving on the same axis (pressing left while moving
    // left won't do anything, and pressing right while moving left
    // shouldn't let you collide with your own body)
  
    // left arrow key
    if (e.which === 37 && snake.dx === 0) {
      snake.dx = -grid;
      snake.dy = 0;
    }
    // up arrow key
    else if (e.which === 38 && snake.dy === 0) {
      snake.dy = -grid;
      snake.dx = 0;
    }
    // right arrow key
    else if (e.which === 39 && snake.dx === 0) {
      snake.dx = grid;
      snake.dy = 0;
    }
    // down arrow key
    else if (e.which === 40 && snake.dy === 0) {
      snake.dy = grid;
      snake.dx = 0;
    }
  });
  
  // start the game
  requestAnimationFrame(loop);
  </script>
  </body>
  </html>
  
  ```
</details>


## Pi-hole

Не забываем про пароль! Тут такая же ситуация как и с Nginx Proxy Manager, можно поднять все эти три сервиса в рамках 
одного .yml, но мне удобнее пилить их. 

**docker-compose.yml**
``` bash
version: "3.8"
services:
    pihole-temp:
      image: pihole/pihole:latest
      container_name: pihole-temp
      environment:
        - WEBPASSWORD=
        #- VIRTUAL_HOST=
      volumes:
        - './.pihole/etc-pihole:/etc/pihole'
        - './.pihole/etc-dnsmasq.d:/etc/dnsmasq.d'
          #ports:
          #- ":53/tcp"
          #- ":53/udp"
          #- ":80/tcp"
      restart: unless-stopped
      networks:
        asgardnet:
          ipv4_address: 13.1.0.6

networks:
  asgardnet:
    external: true
    name: asgardnet

```

Проверяем: http://13.1.0.6/admin

Pi-hole нам нужен в первую очередь как днс сервер, но он может отчасти решать и проблему рекламы.
В целом, интерфейс скажет куда больше чем я смогу тут описать, нужно просто ковыряться, ну или 
глянуть другие туториалы, их много.

## Mkcert

[Здесь](https://gitlab.com/kuznetsov-as/mkcert) дана полная инструкция по установке этой утилиты.
Она нужна нам для генерации сертификатов для нашего домена. В дальнейшем это позволит нам гулять 
по нашим локальным сервисам через https.

Дальше смотрим на эту схему:

![изображение.png](img/poddomen.png)

Если мы хотим однажды выписать один сертификат подходходящий под все наши сервисы, то нужно придумать 
домен второго уровня. Сами сервисы при этом будут лежать на разных поддоменах.

Пример:

firstservice.home.lab

secondtservice.home.lab

Таким образом для создания сертификата используем команду
``` bash
mkcert *.home.lab
```

Если же хотим гулять по сервисам через домены второго уровня, то придется на каждый выпускать свой 
сертификат. Если же не верить мне и все таки попробовать сделать что-то вроде этого:
``` bash
mkcert *.home
```

То сертификат, конечно, он создаст, но сразу вылетит ворнинг:
<span style="color:red">Warning: many browsers don't support second-level wildcards like "*.home"</span>.

И действительно, в дальнейшем, если прицепить такой сертификат к сервису - браузер (как минимум Firefox) 
будет ругаться. 

## Соединяем все

Теперь внимательно на эту схему из которой станет понятно чем мы вообще сейчас будем заниматься:
![изображение.png](img/scheme.png)

В первую очередь нам необходимо сообщить клиенту Wireguard о том, что у нас теперь есть свой DNS 
сервер, для этого необходимо отредактировать файл .conf (можно прямо через клиентское приложение).

Нас интересует именно поле DNC - в него записываем ip указанный в docker-compose Pi-hole.

``` bash
[Interface]
PrivateKey = ***
Address = ***
DNS = 13.1.0.6

[Peer]
PublicKey = ***
PresharedKey = ***
AllowedIPs = ***
Endpoint = ***:51820
```

Отлично, теперь, когда клиент будет вбивать что-то в строку браузера, наш Pi-hole будет сначала проверять, 
нет ли у него соответствующих записей доменных имен и ip. Если нет - дальше будет отрабатывать днс гугла 
(это настраивается в веб панели **Pi-hole: Settings => Upstream DNS Servers**)

Чтобы Pi-hole знал на какой ip перенаправлять клиента - нужно внести ему соответсвующую запись на вкладке 
Local DNS => DNS records. Исходя из схемы, перенапралять все запросы мы будем в наш Nginx Proxy Manager, 
если следовали этой инструкции, то его ip сейчас: 13.1.0.5

В итоге имеем что-то вроде этого:
![изображение.png](img/pihole.png)

Теперь все эти запросы будут попадать в Nginx, так что мы должны сообщить ему что делать с ними дальше.

Во первых, можем сразу создать сертификат (**SSL Certificates => Add SSL Certificate => Custom**). Загружаем туда 
наши файлы .pem (которые сформировали при помощи Mkcert) и даем сертификату имя, для своего удобства.

Во вторых, идем проксировать пришедщий от клиента запрос на нужный адрес, преходим в **Hosts => Proxy Hosts => 
Add Proxy Host** и заполняем данные. Доменное имя указываем как в pihole, Sheme везде оставляем http, хотя по 
факту будет https (вот [тут](https://stackoverflow.com/questions/70640901/nginx-proxy-manager-502-bad-gateway-error-only-on-https-mode) 
объясняют почему так). В хостнейм вместо IP можем спокойно указывать название контейнера заданное внутри 
docker-compose.yml (содержимое директивы container_name в каждом конкретном сервисе docker-compose.yml). Также указываем 
внутренний порт контейнера, на котором все крутится. Итого, для Nginx имеем следующие:

![изображение.png](img/npm.png)

На вкладке SSL указываем имя сертификата, который мы ранее подгружили в Nginx Proxy Manager. Ну и тут же
протыкиваем Force SSL, если нам не нужен доступ по http. 

![изображение.png](img/ssl.png)

Готово, теперь удостоверимся, что на клиенте, с которого имеем доступ ко всем сервисам в локальной сети, установлен 
корневой сертификат (вот [тут](https://gitlab.com/kuznetsov-as/mkcert#%D1%81%D0%BE%D1%85%D1%80%D0%B0%D0%BD%D0%B5%D0%BD%D0%B8%D0%B5-%D0%BA%D0%BE%D1%80%D0%BD%D0%B5%D0%B2%D0%BE%D0%B3%D0%BE-%D1%81%D0%B5%D1%80%D1%82%D0%B8%D1%84%D0%B8%D0%BA%D0%B0%D1%82%D0%B0-%D0%B4%D0%BB%D1%8F-%D0%BA%D0%BB%D0%B8%D0%B5%D0%BD%D1%82%D0%BE%D0%B2) можно все об этом прочитать) и 
теперь можем идти по адресу https://mynginx.evil.corp/ и начинать играть в нашу змейку.

![изображение.png](img/snake.png)

В итоге у меня Nginx Proxy Manager выглядит так:

![изображение.png](img/proxy.png)

## Wireguard client on other machine

Рано или поздно возникнет желание включить в нашу приватную сеть контейнер развернутый на другой физической машине. Использовать на ней сеть asgardnet уже не получится, ведь
мы создавали ее в рамках другого демона докера и на других физических машинах ее нет. У этой проблемы есть решение - 
в такой ситуации нам будет необходимо подключить контейнер, расположенный на другой физической машине, как клиент нашего wireguad сервера. О том, как это сделать
на конкретном примере я писал в инструкции к Wireguard [здесь](https://gitlab.com/kuznetsov-as/box#%D0%BF%D0%BE%D0%B4%D0%BD%D0%B8%D0%BC%D0%B0%D0%B5%D0%BC-%D0%BA%D0%BB%D0%B8%D0%B5%D0%BD%D1%82%D0%B0-%D0%B2%D0%BD%D1%83%D1%82%D1%80%D0%B8-%D0%B4%D0%BE%D0%BA%D0%B5%D1%80-%D0%BA%D0%BE%D0%BD%D1%82%D1%80%D0%B5%D0%B9%D0%BD%D0%B5%D1%80%D0%B0-%D0%BD%D0%B0-%D0%B1%D0%B0%D0%B7%D0%B5-%D0%BE%D0%B1%D1%80%D0%B0%D0%B7%D0%B0-ubuntu2004).

Способ выше подходит, если мы хотим подключить в нашу приватную сеть один контейнер, но может возникнуть ситуация, 
когда таких контейнеров на другой машине может быть много. В этом случае подключать каждый из них как отдельный клиент wireguard представляется
не самой хорошей идеей. В таком случае сначала мы поднимем отдельный контейнер, который будет отвечать исключительно за соединение с wireguard сервером:

**docker-compose.yml**
``` bash
version: "3.8"

services:
  wg-client:
    build: .
    container_name: wg-client
    restart: unless-stopped
    cap_add:
    - NET_ADMIN
    - SYS_MODULE
    sysctls:
    - net.ipv4.conf.all.src_valid_mark=1
    volumes:
    - ./wg-config/:/etc/wireguard
```
**Dockerfile**
``` bash
FROM ubuntu:22.04
RUN apt-get -y update && apt-get -y upgrade
RUN apt-get -y install wireguard wireguard-tools iproute2
COPY init.sh /scripts/init.sh
RUN ["chmod", "+x", "/scripts/init.sh"]
ENTRYPOINT ["/scripts/init.sh"]
```
**init.sh**
``` bash
#!/bin/bash
echo "Start client"
wg-quick up wg0
#Не понимаю почему, но bash или /bin/bash не работает, поэтому используем:
tail -f /dev/null 
```
И не забываем положить в папку wg-config (ту самую, которую указали в volumes) файл конфигурации клиента. 
Выглядеть он должен по аналогии c примером ниже. DNS не указываем, в AllowedIPs указываем подсеть клиентов wireguard:

**wg0.conf**
``` bash

[Interface]
PrivateKey = 
Address = 10.1.0.5/24


[Peer]
PublicKey = 
PresharedKey = 
AllowedIPs = 10.1.0.0/24
PersistentKeepalive = 25
Endpoint = 
```
После этого необходимо выполнить билд и поднять контейнер:

``` bash
docker-compose build && docker-compose up -d 
```
Теперь подключим к этому клиенту сторонний сервис, развернутый с помощью другого docker-compose, допустим это будет nginx.
Особое внимание обращаем на network_mode, в нем мы указываем имя контейнера - клиента wireguard:

**docker-compose.yml**
``` bash
version: "3.5"

services:

  nginx:
    image: nginx
    container_name: nginx
    restart: unless-stopped
    #ports:
    #  - "9090:80"
    volumes:
      - ./log:/var/nginx/log:ro
      - ./data:/usr/share/nginx/html:ro
    network_mode: "container:wg-client"
```
Не забываем положить какой-нибудь index.html в папку data:

**index.html**
``` bash
<!DOCTYPE html>
<html>
    <head>
        <title>Example</title>
    </head>
    <body>
        <p>This is an example of a simple HTML page.</p>
    </body>
</html>
```
После запуска сможем по ip адресу клиента wireguard получить доступ к нашей html странице на 80 порту.

## PS

С прочими сервисами действуем по аналогии. Единственное, важно понимать, что теперь мы можем удалить ipv4_address
из .yml файлов у всех контейнеров кроме Pi-hole и Nginx Proxy Manager. Это связано с тем, что веб интерфейс Pi-hole
требуется настоящий Ip адрес Nginx Proxy Manager, а конфигурационному файлу (.conf) клиента Wireguard требуется настоящий 
Ip адрес DNS сервера (нашего Pi-hole).

Также в docker-compose.yml содержащий наш Pi-hole есть переменная VIRTUAL_HOST, в нее стоит внести наш домен 
(У меня это VIRTUAL_HOST=pihole.evil.corp). Это нужно, чтобы нас автоматически редиректило в https://pihole.evil.corp/admin 
если будем пытаться зайти в https://pihole.evil.corp/